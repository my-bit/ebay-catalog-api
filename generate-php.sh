#!/bin/bash

openapi-generator generate -i commerce_catalog_v1_beta_oas3.yaml -g php -o . --additional-properties=apiPackage=Api,invokerPackage=MyBit\\Ebay\\Catalog,artifactVersion=1.1.0,composerPackageName=my-bit/ebay-catalog-api